jQuery(document).ready(function() {
  var base_path = Drupal.settings.basePath;
  
  // Enable the tabs.
  var jQuerytabs = jQuery("#node_tabs > ul#node_tabs_nav").tabs();
  
  // Make the edit tabs sortable.
  jQuery(".edit_node_tabs_nav").sortable();
	jQuery(".edit_node_tabs_nav").disableSelection();
	
	jQuery(".edit_node_tabs_nav").bind("sortupdate", function(event, ui) {
	  var position;
	  
	  jQuery(this).data("new_position", jQuery(this).sortable("toArray"));
    position = jQuery(this).data("new_position");
    
    // Remove the first item from the position array.
    position.shift();
    
    // Loop through each item in the position array.
    jQuery("#node_tabs_nav li").each(function(index) {
      var whatsit;
      whatsit = jQuery("#node_tabs_nav li:eq("+ index +")").attr("id");
      
      if (whatsit) {
        jQuery("#tabs-"+ whatsit +" .tab_weight").val(index);
      }
    });
  });
  
  var this_tab_count = jQuerytabs.tabs("length")+1;
  
  // Get the tab count.
  var tab_count;
  jQuery.getJSON(base_path +'?q=node_tabs/max', function(d) {
       tab_count = d;
  });
  
  // Add a new tab.
  jQuery("#add_new_tab").click(function() {
      jQuery.getJSON(base_path +"?q=node_tabs/update&count="+tab_count, function(data) {
       alert(data);
      jQuerytabs.tabs("add", "#tabs-"+ tab_count, "New tab "+ this_tab_count);
      jQuery("#tabs-"+ tab_count).append('<div class="form-item"><label for="node_tabs_title_new_'+ tab_count +'">Title: </label><input type="text" name="node_tabs_title_new_'+ tab_count +'" id="node_tabs_title_new_'+ tab_count +'"/></div><input class="tab_weight" type="hidden" value="'+ tab_count +'" name="node_tabs_weight_new_'+ tab_count +'"><div class="form-item"><label for="node_tabs_content_new_'+ tab_count +'">Description: </label><textarea id="node_tabs_content_new_'+ tab_count +'" class="form-textarea" name="node_tabs_content_new_'+ tab_count +'" rows="5" cols="60"></textarea></div>');
      
      jQuery("#node_tabs_title_new_"+ tab_count).val("New tab "+ this_tab_count);
      
      console.log(data);
      tab_count++;
      this_tab_count++;
    });
    return false;
  });
});
