Node Tabs Installation / Configuration Instructions

Contents
1. Introduction
2. Requirements
3. Installation
4. Configuration
5. Known issues
6. In the field
7. Authors

1. Introduction

Node tabs is a module that allows you to assign tabbed content to nodes.
The module creates a block called Node tabs that you can add to a region or panel.
You can also call the tabs using the following function: <?php print node_tabs_generate_tabs(); ?>

2. Requirements

Node tabs requires that jQuery UI be installed. You can download jQuery UI from
http://drupal.org/project/jquery_ui.

3. Installation

Follow the standard Drupal module installation procedure. Put the module in the 
modules folder (sites/all/modules or sites/<custom>/modules if you are using a 
multisite setup). Then enable Node tabs in the module admin section (admin/build/modules).

4. Configuration

Node tabs has an admin page where you choose which content types are allowed to 
contain tabs (admin/settings/node_tabs).

Node tabs also creates two new user permissions:
  a. administer node tabs
  b. add / edit node tabs

You will have to assign these permissions to roles before your users will be able 
to start using Node tabs.

5. Known issues

  a. When adding a new tab you cannot choose the input format before saving the node.

6. In the field

The node tabs module is in use on the following sites:
  a. Unitemp (http://www.unitemp.com)
  b. Tabateq (http://www.tabateq.com)

If you would like you site to be listed here as well please send an email to info@amoebasys.biz

7. Authors

Node tabs has been developed by Amoebasys (http://amoebasys.biz) and sponsored 
by MediaMachine (http://www.mediamachine.co.za)
